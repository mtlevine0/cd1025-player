This application is a simple replacement for the now defunt CD1025 streamer.
Due to royalties being too high CD1025 had to remove their streamer.  This 
streamer works by querying the CD1025 website's currently playing feature
and building a playlist.  The application currently is planned to utilize 
YouTube for music needs.
